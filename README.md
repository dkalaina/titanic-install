# Cluster usage tutorial
# Connection to the cluster:
- through host seringa (ssh.saclay.inria.fr)
- then on machine titanic
- you need to forward your ssh key ! (-A)
- you can configure your .ssh/config so that ssh titanic directly connects you to titanic.
  For me:

```
Host inria 
    HostName ssh.saclay.inria.fr
    User dkalaina
    IdentityFile ~/.ssh/inria

Host titnic
    ProxyCommand ssh -q -A inria nc titanic 22 
    User dkalaina
```

# Enabling cluster account
- Come and see us !

# Details on cluster specs, management, storage
- The cluster: titanic, 5 nodes titanic-[1-5], baltic-1
1. For titanic-[1-5]: 48 cpu threads, 4 GPU cards (GTX 1080Ti, 12Go)
2. For baltic-1: 48 cpu threads, 4 GPU cards (Titan black, 6Go)
- storage: 
1. shared homedir, but with very low storage capacity: 200Go for
all users. Don't store your data and your logs here! Only code.
2. nodes all have local storage. If you need storage, come and see
us, so that we can create dedicated folders for you on the nodes.
Note: You can always create a symbolic link to your storage directory
on nodes in your homedir! (for me ln -s /data/titanic_3/users/ctallec/data ~/data)
3. In the future, we plan on having a shared directory for usual datasets.
This is not yet implemented.

# How to send jobs with all the options, queues
- Check the tutorial on [conda and python here](Jobs_conda.md)
- When using python:
1. Use conda's virtual environments!
2. Recent versions of most deep learning frameworks are preinstalled on
   the machines. However they are not directly linked to your conda environments.
   To allow your virtual environments to see them, use the script provided by
   Diviyan on the git (follow the instructions).
3. If you need other frameworks/libs, you have two solutions. Either the lib is
   not too heavy and you can install it locally in your venv. Otherwise, come and see
   us.
- To directly launch jobs on nodes on the cluster you need to put them in an executable
  file. Whichever executable file is a valid job to send. For example, launching the following
  executable script:

  ~/script.sh

```bash
  #!/bin/sh

  echo "Bonjour"
```

  Is done with the command
  
```bash
  $ sbatch script.sh
```

- If you need specific cpu ressources, you can specify them. Check SLURM documentation for more
  information on the subject https://slurm.schedmd.com/quickstart.html.
- To use the GPUs, you need to specify the number of GPUs you want. For example, if you want to
  launch the previous script using 2 GPUs, use

```bash
  $ sbatch --gres=gpu:2 script.sh
```

  Alternatively you can specify the requirements for your script (as well as any other options)
  directly in the bash script.

```bash
  #!/bin/sh
  #SBATCH --gres=gpu:2

  echo "Bonjour"

  $ sbatch script.sh
```
  Check out https://slurm.schedmd.com/quickstart.htm://slurm.schedmd.com/quickstart.html for an
  exhaustive list of options.

- If you want a specific type of GPU, use the -C option (short for --constraint=)
  (pascal for GTX1080Ti and kepler for Titan Black).

```bash
  $ sbatch --gres=gpu:2 -C pascal script.sh
```

- When GPUs are allocated, you still need to make sur your code is able to run on
  a GPU. Furthermore, allocated GPUs are given by the environment variable $CUDA_VISIBLE_DEVICES.
  Note: Most deeplearning framework do that by default.
  Warning: Never use GPus that were not allocated.

- Queues: Slurm implements a queue system for fair sharing of ressources. Each queue has a different
  allocation behavior. Currently, we have two different queues: a default (priority) queue, and a 
  besteffort queue. 
1. On the default queue, jobs are not killable by other users. If you launch a
   job on the default queue, it will only stop when it has terminated, or if you cancel it by
   yourself. 
2. On the besteffort queue, jobs are killable. Namely, if a user wishes to allocate a job
   on the default queue, while his ressource requirements are not met, but killing a job
   in the besteffort queue would allow his job to run, then the job in the besteffort queue
   is cancelled and requeued. Note that requeuing a job does not preserves its state. You
   need to take care of that yourself.

- Users are only allowed a total of 4 GPUs on the default queue (GPU, not nodes!). If you
  want to launch jobs on more than 4 GPUs, you need to use the besteffort queue.

# Monitoring your jobs
- You can first check your job state (WAITING, RUNNING, ...) using squeue.
- You can check node states using sinfo.
- To further debug/monitor/... for each launched job, you have an associated
  log file, (slurm-(job-id).out by default, can be specified with the options
  -e and -o, for stderr and stdout). Plain old logging in a specific log directory,
  on your personal storage, is still the preferred method when dealing with heavy
  logs.
  Note: a cluster monitoring application might be used in the not so distant future
  (might be netdata, checkout citronelle.lri.fr:19999).

# Interactive sessions
- For debug purposes, you can use srun with the option --pty bash to launch an
  interactive session on the cluster. You can specify most of the previously
  mentionned options in your srun call. Be aware that if not specified, the time
  limit is set to 1 hour.
- Thanks to Victor Estrade, scripts are available to run jupyter notebooks directly
  on the cluster. Checkout the git for the scripts and documentation (or go directly
  to him).

# Bad practices to avoid
- Directly connecting to the cluster nodes (not titanic, but for example titanic-2).
- Changing $CUDA_VISIBLE_DEVICES, not using it, ...
- Using titanic-master as a computational node. This is not the way to go, and may
  break down he whole cluster if you put too much load on master, as it is a virtual
  machine, in charge of the nodes synchronization.
- Using the homedir as a data storage facility. Again, homedir is a 200Go shared directory:
  only use it to store your codebase.

# Reporting issues and updates
- To report issues, use primarily the slack channel #titanic-users.
- The users will be notified of the updates through the mailing list titanic-users@inria.fr
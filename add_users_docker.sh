#!/bin/bash

while read line
do
    echo $line
    if id -nG "$line" | grep -qw "docker"; then
	echo ""
    else
	sudo usermod -aG docker $line
    fi
    
done < /home/tao/dkalaina/titanic-install/users.yml

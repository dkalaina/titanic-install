from fabric.api import *
import getpass
import sys
# from fabric import Connection
# env.gateway = 'dkalaina@titanic'
# env.gateway = [, '']
# import argparse

# parser = argparse.ArgumentParser(description='Parser')
# parser.add_argument("cmd", type=str, help='cmd to run')
# args = parser.parse_args()
def connect(host):

    return Connection(host, gateway=Connection('dkalaina@titanic', gateway=Connection('dkalaina@ssh.saclay.inria.fr')))


env.hosts = ['republic-6', 'republic-5','republic-1',
             #'republic-3',
	     'republic-4', 'republic-2', 'titanic-1', 'titanic-2', 'titanic-3','titanic-4',
             'titanic-5', 'baltic-1','pacific-1']  # '']
env.password = getpass.getpass('sudo password: ')
python_version="2"
print(sys.argv)
@task()
def install_python():
    run('export HTTP_PROXY=titanic:80')
    run('export HTTPS_PROXY=titanic:443')
    run('sudo apt-get install python3')


@task()
def install_python_libs():
    
    run('export HTTP_PROXY=titanic:80')
    run('export HTTPS_PROXY=titanic:443')
    with settings(prompts={'Souhaitez-vous continuer ? [O/n] ': 'O'}):
        sudo('apt-get install python'+ str(python_version)+'-pip')
        sudo('/usr/bin/python2 -m pip install numpy scipy scikit-learn pandas joblib matplotlib --no-cache-dir')
        #sudo('/usr/bin/python' + str(python_version) + ' -m pip install http://download.pytorch.org/whl/cu80/torch-0.1.12.post2-cp35-cp35m-linux_x86_64.whl torchvision --no-cache-dir')
        sudo('/usr/bin/python2 -m pip install http://download.pytorch.org/whl/cu80/torch-0.1.12.post2-cp27-none-linux_x86_64.whl torchvision')

        
@task()
def install_bazel():
    
    run('export HTTP_PROXY=titanic:80')
    run('export HTTPS_PROXY=titanic:443')
    with settings(prompts={'Souhaitez-vous continuer ? [O/n] ': 'O'}):
        sudo('apt-get install openjdk-8-jdk')
        sudo('echo "deb [arch=amd64] http://storage.googleapis.com/bazel-apt stable jdk1.8" | tee /etc/apt/sources.list.d/bazel.list')
        sudo('curl https://bazel.build/bazel-release.pub.gpg | apt-key add -')
        sudo('apt-get update') 
        sudo('apt-get install bazel')
        sudo('apt-get upgrade bazel')

@task
def install_cudnn():
    # env.password = getpass.getpass('sudo password: ')
    run('export HTTP_PROXY=titanic:80')
    run('export HTTPS_PROXY=titanic:443')
    with cd("/tmp"):
        run('cp /data/titanic_1/cudnn-8.0.tgz .')
        # run('wget https://drive.google.com/open?id=')
        run('tar zxf cudnn-8.0.tgz')
        with cd('cuda'):
            sudo('mv lib64/* /usr/local/cuda-8.0/lib64/')
            sudo('mv include/* /usr/local/cuda-8.0/include')
    
    
@task
def compile_tensorflow():

    run('export HTTP_PROXY=titanic:80')
    run('export HTTPS_PROXY=titanic:443')
    with settings(prompts={'Souhaitez-vous continuer ? [O/n] ': 'O'}):
        # sudo('apt-get install git python' + str(python_version) + '-numpy python' + str(python_version) + '-dev python' + str(python_version) + '-pip python' + str(python_version) + '-wheel')
        sudo('apt-get install git python-numpy python-dev python-pip python-wheel')
    with cd("/tmp"):
        run('mkdir bazel_cache')
        run('git clone https://github.com/tensorflow/tensorflow')
        with cd('tensorflow'):
             run('pwd')
             run('git checkout r1.3')
             with settings(prompts={'Souhaitez-vous continuer ? [O/n] ': 'O'}):
                 sudo('apt-get install libcupti-dev')
             with settings(prompts={'Please specify the location of python. [Default is /usr/bin/python]: ':'/usr/bin/python' + str(python_version) + '',
                                    'Please input the desired Python library path to use.  Default is [/usr/local/lib/python' + str(python_version) + '.7/dist-packages]':'',
                                    'Do you wish to build TensorFlow with MKL support? [y/N] ':'y',
                                    'Do you wish to download MKL LIB from the web? [Y/n] ':'y',
                                    'Please specify optimization flags to use during compilation when bazel option "--config=opt" is specified [Default is -march=native]: ': '',
                                    'Do you wish to use jemalloc as the malloc implementation? [Y/n] ':'',
                                    'Do you wish to build TensorFlow with Google Cloud Platform support? [y/N] ':'',
                                    'Do you wish to build TensorFlow with Hadoop File System support? [y/N] ':'',
                                    'Do you wish to build TensorFlow with the XLA just-in-time compiler (experimental)? [y/N] ':'',
                                    'Do you wish to build TensorFlow with VERBS support? [y/N] ':'',
                                    'Do you wish to build TensorFlow with OpenCL support? [y/N] ':'',
                                    'Do you wish to build TensorFlow with CUDA support? [y/N] ':'y',
                                    'Do you want to use clang as CUDA compiler? [y/N] ':'',
                                    'Please specify the CUDA SDK version you want to use, e.g. 7.0. [Leave empty to use system default]: ':'8.0',
                                    'Please specify the location where CUDA 8.0 toolkit is installed. Refer to README.md for more details. [Default is /usr/local/cuda]: ':'/usr/local/cuda-8.0',
                                    'Please specify which gcc should be used by nvcc as the host compiler. [Default is /usr/bin/gcc]: ':'',
                                    'Please specify the cuDNN version you want to use. [Leave empty to use system default]: ':'6.0.21',
                                    'Please specify the location where cuDNN 6.0.21 library is installed. Refer to README.md for more details. [Default is /usr/local/cuda-8.0]: ':'',
                                    '[Default is: "3.5,5.2"]: ':'3.5'
             }):
                 run('./configure ') # python=/usr/bin/python' + str(python_version) + ',all default ,Cuda =y , /usr/local/cuda-8.0 , cudnn 6.0.21, defaults
             run('bazel --output_base=/tmp/bazel_cache build --config=opt --copt=-march="broadwell" --config=cuda //tensorflow/tools/pip_package:build_pip_package ')
             run('bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp/tensorflow_pkg')
             sudo('/usr/bin/python' + str(python_version) + ' -m pip install /tmp/tensorflow_pkg/tensorflow-1.3.1-cp27-cp27m-linux_x86_64.whl ')
             run('rm -rf /tmp/bazel_cache')
             run('rm -rf /tmp/tensorflow')
@task()
def bla():
    run('echo LALALALA')

@task()
def update():
    sudo('/usr/bin/python' + str(python_version) + ' -m pip install --upgrade numpy scipy scikit-learn pandas joblib matplotlib')
    sudo('/usr/bin/python' + str(python_version) + ' -m pip install http://download.pytorch.org/whl/cu80/torch-0.2.0.post3-cp35-cp35m-manylinux1_x86_64.whl torchvision --no-cache-dir')
    sudo('/usr/bin/python2 -m pip install --upgrade numpy scipy scikit-learn pandas joblib matplotlib')
    sudo('/usr/bin/python2 -m pip install http://download.pytorch.org/whl/cu80/torch-0.2.0.post3-cp27-cp27mu-manylinux1_x86_64.whl torchvision --no-cache-dir')

@task()
def install_keras():
    # sudo('/usr/bin/python' + str(python_version) + ' -m pip install keras')
    # sudo('/usr/bin/python2 -m pip install keras')
    sudo('apt-get install unzip -y')

@task()
def sudo_cmd(cmd):
    # sudo('/usr/bin/python' + str(python_version) + ' -m pip install keras')
    # sudo('/usr/bin/python2 -m pip install keras')
    sudo(cmd)

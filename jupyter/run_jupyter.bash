#!/bin/bash

OPTIONS=""
OPTIONS="$OPTIONS --account=tau"
OPTIONS="$OPTIONS --job-name=jupyter-notebook"
# OPTIONS="$OPTIONS --output=jupyter.log"
OPTIONS="$OPTIONS --ntasks=1"
OPTIONS="$OPTIONS --cpus-per-task=15"
OPTIONS="$OPTIONS --gres=gpu:1"
OPTIONS="$OPTIONS -t 20:00:00"             # max runtime is 9 hours
OPTIONS="$OPTIONS -C tesla"
# OPTIONS="$OPTIONS -p besteffort"

echo "srun $OPTIONS jupyter.sh" 
source activate py35
srun $OPTIONS -C pascal jupyter.bash
